<?php get_header(); ?>

<main>
	<section>
		<div class="wrap xl-center container">
			<div class="col">


				<h1>Page not found</h1>
				<p>Sorry, we couldn't find what you're looking for.</p>


			</div>
		</div>
	</section>
</main>

<?php get_footer(); ?>